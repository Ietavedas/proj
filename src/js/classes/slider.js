import Swiper from 'swiper';

export default class Sliders{
    constructor(){
        this.el = $('.programs');
        this.el2 = $('.constructor_mobile');
        this.el3 = $('.compos_mobile');
        

        this.init()
    }

    init(){

        const slider = new Swiper(this.el, {
            // direction: 'vertical',
            mousewheel: {
				releaseOnEdges: true,
			},
			touchReleaseOnEdges: true,
            slidesPerView: 5,
            spaceBetween: 30,
            breakpoints: {
                1450: {
                    slidesPerView: 4,
                },
                1024: {
                    slidesPerView: 3,
                },
                600: {
                    slidesPerView: 2,
                },
                400: {
                    slidesPerView: 1
                }
            }
        })

        const slider_constructor = new Swiper(this.el2, {
            // direction: 'vertical',
            mousewheel: {
				releaseOnEdges: true,
			},
			touchReleaseOnEdges: true,
            slidesPerView: 1
        })

        const slider_compos = new Swiper(this.el3, {
            // direction: 'vertical',
            mousewheel: {
				releaseOnEdges: true,
			},
			touchReleaseOnEdges: true,
            slidesPerView: 1
        })
    }
}