import 'jscrollpane/script/jquery.jscrollpane.min';
import 'jscrollpane/script/jquery.mousewheel';

export default class customScroll{
    constructor(){
        this.elem = $('.compos__block');

        this.init();
    }

    init(){
        this.elem.hover(function(){
            let heightText = $(this).find('.compos__text').outerHeight() + 30;
            let heightBlock = $(this).outerHeight();
            if(heightText > heightBlock){
                $(this).jScrollPane()
            }
        })
    }
}