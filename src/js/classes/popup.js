export default class Popup{
    constructor(){
        this.popup = '.js-popup';

        this.init();
    }

    init(){
        $(this.popup).on('click', function(event){
            event.preventDefault();

            $('.overflow').css({
                'display': 'block'
            })
            $('.overflow').animate({
                'opacity': 1
            }, 500)

            $('.left_popup').animate({
                'right': 0
            }, 500)

            $('.left_popup__form').css('display', 'block');
        })

        $('.overflow').on('click', function(){
            $('.js-team').removeClass('team--active')
            setTimeout(function(){
                $('.overflow').css({
                    'display': 'none'
                })
            }, 500)
            $(this).animate({
                'opacity': 0
            }, 500)

            $('.left_popup').animate({
                'right': '-100%'
            }, 500)

            $('.left_popup__form').css('display', 'none');
            $('.left_popup').find('.left_popup__text').html('')
        })

        $('.review__link').on('click', function(event){
            event.preventDefault();

            $('.overflow').css({
                'display': 'block'
            })
            $('.overflow').animate({
                'opacity': 1
            }, 500)

            $('.left_popup').animate({
                'right': 0
            }, 500)

            let text = $(this).data('text');

            $('.left_popup').find('.left_popup__text').html(text)

        })

        $('.icon-close').on('click', function(){
            $('.js-team').removeClass('team--active')
            setTimeout(function(){
                $('.overflow').css({
                    'display': 'none'
                })
            }, 500)
            $(this).animate({
                'opacity': 0
            }, 500)

            $('.left_popup').animate({
                'right': '-100%'
            }, 500)

            $('.left_popup__form').css('display', 'none');
            $('.left_popup').find('.left_popup__text').html('')

        })
    }
}