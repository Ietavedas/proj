import 'fullpage.js'
// import './fp';

class Fp{
    constructor(){
        this.selector = '#fullpage';

        if ($(this.selector).length > 0) {

			this.getResize();

			$(window).on('resize', this.getResize.bind(this));
			
		}

        // this.init();
    }

    getResize(){

		const isInitClass = $('.fp-enabled');
		let windowWidth = $(window).outerWidth();

		if (windowWidth < 1024) {

			if (isInitClass.length > 0) {

				$.fn.fullpage.destroy('all');
				
			}

		}else{

			this.init();

		}

	}

    init(){
        const fp = $(this.selector);
        
        fp.fullpage({
            sectionSelector: '.screen',
            // scrollHorizontally: true,
            onLeave: function(origin, destination, direction){
                // console.log(arguments)
                
                if(origin == 1 && direction == 'down'){
                    $.fn.fullpage.setAllowScrolling(false, 'down');
                }
                if(origin == 2 && direction == 'up'){
                    $.fn.fullpage.setAllowScrolling(true, 'down');
                }
                if(origin == 2 && direction == 'down'){
                    $.fn.fullpage.setAllowScrolling(true);
                }
                if(origin == 3 && direction == 'up'){
                    $.fn.fullpage.setAllowScrolling(false, 'up');
                }
                if(origin == 3 && direction == 'down'){
                    $.fn.fullpage.setAllowScrolling(false, 'down');
                }
                if(origin == 4 && direction == 'up'){
                    $.fn.fullpage.setAllowScrolling(true, 'down');
                }
                if(origin == 5 && direction == 'up'){
                    $.fn.fullpage.setAllowScrolling(false, 'up');
                }
                if(origin == 5 && direction == 'down'){
                    $.fn.fullpage.setAllowScrolling(false, 'down');
                }
                if(origin == 6 && direction == 'up'){
                    $.fn.fullpage.setAllowScrolling(true, 'down');
                }
                if(origin == 6 && direction == 'down'){
                    setTimeout(function(){
                        $.fn.fullpage.setAllowScrolling(true, 'up');
                    })
                }

                if( destination % 2 === 0 ){
                    $('.menu').addClass('menu--black');
                    $('.header__phone').addClass('header__phone--black');
                    $('.icon-logo').addClass('icon-logo--black');
                }else{
                    $('.menu').removeClass('menu--black');
                    $('.header__phone').removeClass('header__phone--black');
                    $('.icon-logo').removeClass('icon-logo--black');
                }

                $('.menu__link').removeClass('menu__link--active');
                $('.menu__link[data-screen=' + destination + ']').addClass('menu__link--active');
            }
        });

        $(document).on('upTrue', function() {
            console.log('upTrue');
            $.fn.fullpage.setAllowScrolling(true, 'up');
        });

        $(document).on('upFalse', function() {
            console.log('upFalse');
            $.fn.fullpage.setAllowScrolling(false, 'up');
        });

        $(document).on('downFalse', function() {
            console.log('down');
            $.fn.fullpage.setAllowScrolling(false, 'down');
        });

        $(document).on('downTrue', function() {
            console.log('downTrue');
            $.fn.fullpage.setAllowScrolling(true, 'down');
        });
    }
}

export default Fp;