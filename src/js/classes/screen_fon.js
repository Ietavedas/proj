export default class Screen_fon{
    constructor(){
        this.block = '.compos__block';
        this.isAnimating = false;

        this.bg;

        this.init()
    }
    
    init(){ 
               
        $(this.block).on('mouseenter', (event) => {

            clearTimeout(this.bg);

            let count = this.count(event);
            this.render(count)
        });

        $(this.block).on('mouseleave', (event) => {
            $('.screen__fon').removeClass('screen__fon--amimate')
        });
        
    }

    count({currentTarget}){
        const selector = $(currentTarget);

        return selector.data('fon');
    }

    render(count){
        $('.screen__fon').append(`<div class="screen__fon--amimate" style="background: url(img/3bg-fon-${count}.jpg) center center no-repeat" ></div>`)
        
        let x = $('.screen__fon').find('div');
        
        this.bg = setTimeout( () => {

            $('.screen__fon').css({
                background: `url(img/3bg-fon-${count}.jpg) center center no-repeat`
            })

        }, 900 )

        setTimeout( () => {
            x.remove();
        }, 3000 )
    }
}