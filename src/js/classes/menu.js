import SliderConstructor from './slider_constructor';

export default class Menu{
    constructor(){
        // this.menu = document.querySelector('.menu');
        // this.containers = document.querySelectorAll('.screen');
        this.menu = '.js-menu';
        this.logo = '.js-logo';

        this.init();
        // this.controller()
    }

    controller(){
        
        const menu = this.menu;
        
        let menuNew = this.cloneMenu(menu)
        let addMenu = this.addMenu(menuNew)
        
        this.modify()

        $('.menu').animate({
            'opacity': 1
        }, 500)
        // console.log($('.menu').length)
        // for(let i = $('.menu').length; i > -1; i--){
        //     console.log(i)
            
        // }
        $('.menu').eq(6).find('.menu__link').eq(0).addClass('menu__link--active');
        $('.menu').eq(5).find('.menu__link').eq(1).addClass('menu__link--active');
        $('.menu').eq(4).find('.menu__link').eq(2).addClass('menu__link--active');
        $('.menu').eq(3).find('.menu__link').eq(3).addClass('menu__link--active');
        $('.menu').eq(2).find('.menu__link').eq(4).addClass('menu__link--active');
        $('.menu').eq(1).find('.menu__link').eq(5).addClass('menu__link--active');
    }

    cloneMenu(menu){
        return menu.cloneNode(true);
    }

    addMenu(menuNew){
        let menu = menuNew.outerHTML;
        this.containers.forEach( (element) => {
            
            element.insertAdjacentHTML('beforeend', menu)
        })
        this.menu.remove();
    }

    render(){
        return (
            `<ul class="menu">
            <li class="menu__item"><a class="menu__link" href="">работать с нами</a></li>
            <li class="menu__item"><a class="menu__link" href="">кейсы</a></li>
            <li class="menu__item"><a class="menu__link" href="">команда</a></li>
            <li class="menu__item"><a class="menu__link" href="">процесс</a></li>
            <li class="menu__item"><a class="menu__link" href="">составляющие</a></li>
            <li class="menu__item"><a class="menu__link" href="">преимущества</a></li>
          </ul>`
        )
    }

    modify(){
        this.menu = document.querySelectorAll('.menu');
        this.menu.forEach( (element, index) => {
            index % 2 != 0 ? element.classList.add('menu--black') : false
        } )
    }

    init(){

        const self = this;

        const menu = $(this.menu).find('.menu__link');

        menu.on('click', function(event){
            event.preventDefault();
            
            const toSlide = $(this).data('screen');

            $(this).addClass('menu__link--active').siblings().removeClass('menu__link--active');

            if(toSlide == 2){
                // self.constructorBack();
                if($('.collected').length == 1){
                    $(document).trigger('upTrue');
                }else{
                    $(document).trigger('upFalse');
                }

                if($('.collected').length == 6){
                    $(document).trigger('downTrue');
                }else{
                    $(document).trigger('downFalse');
                }
                
            }
            // if(toSlide == 4){
            //     if($('.stage_dots').find('.swiper-pagination-bullet').first().hasClass('swiper-pagination-bullet-active')){
            //         $(document).trigger('upTrue');
            //         // $(document).trigger('downFalse');
            //     }else{
            //         if($('.stage_dots').find('.swiper-pagination-bullet').last().hasClass('swiper-pagination-bullet-active')){
            //             // $(document).trigger('upFalse');
            //             $(document).trigger('downTrue');
            //         }else{
            //             $(document).trigger('upFalse');
            //             $(document).trigger('downFalse');
            //         }
            //     }
            // }

            $.fn.fullpage.moveTo(toSlide);
        })

        $(this.logo).on('click', function(){
            $.fn.fullpage.moveTo(1);
            $('.menu__link').removeClass('menu__link--active');
        })

    }

    constructorBack(){

        // App.SliderConstructor.coord = [];
        // App.SliderConstructor.current = null;
        // App.SliderConstructor.next = null;

        $('.constructor-left').animate({
            width: '70%',
            opacity: 1
        })
        $('.constructor__item:not(:first)').removeAttr('style');
        // console.log($(`.constructor__item`)[0])
        // $($('.constructor__item')[0]).css({
        //     left: 0
        // })
        // $($('.constructor__item')[1]).css({
        //     left: '390px'
        // })
        // $($('.constructor__item')[2]).css({
        //     left: '780px'
        // })
        // $($('.constructor__item')[3]).css({
        //     left: '1170px'
        // })
        // $($('.constructor__item')[4]).css({
        //     left: '1560px'
        // })
        // $($('.constructor__item')[5]).css({
        //     left: '1950px'
        // })

        // $('.constructor__item:not(:first)').removeClass('collected');
        $($('.constructor__item')[0]).addClass('first');
        // $($('.constructor__item')[1]).addClass('current');
        // $($('.constructor__item')[2]).addClass('next');

        App.SliderConstructor = new SliderConstructor();
        // App.SliderConstructor.step = 1;
        // console.log(App.SliderConstructor)

        $(document).trigger('upTrue');
        $(document).trigger('downFalse');
    }
}
