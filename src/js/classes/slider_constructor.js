export default class SliderConstructor {
	constructor() {
        this.id = $('#constructor');
		this.el = '[data-constructor]';
		this.elWrapper = '[data-constructor-wrapper]';
		this.elItem = '[data-constructor-item]';
		
		this.current = null;
		this.next = null;
		
		this.coords = [];
		this.itemsCount = $(this.elItem).length;
		
		this.isAnimating = false;
		this.step = 0;
		
		this.width = 360;
		this.margin = 30;

        this.init();
		
	}
	
	init() {
		this.current = $(this.el).find(`${this.elItem}:first`).next();
		this.next = this.current.next();
		$(this.el).find(`${this.elItem}:first`).addClass('collected').next().addClass('current').next().addClass('next');
		
		this.setCoords(this.width, this.margin);

		this.events();

		
	}
	
	getDirection(event) {
		// let delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
		let delta = parseInt(event.deltaY || event.detail || event.wheelDelta);
		
		if (delta >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	firstStep() {
		$(this.el).addClass('started');
		$(`${this.elItem}:first`).removeClass('first');
		this.id.find('.constructor-left').animate({
			'width': 0,
			'opacity': 0
		}, 500)
		this.isAnimating = true;
		setTimeout(() => {
            this.isAnimating = false;
		}, 500);
		this.step = 1;
		
	}
	
	lstStep() {
		$(this.el).removeClass('started');
		$(`${this.elItem}:first`).addClass('first');
		this.id.find('.constructor-left').animate({
			'width': '50%',
			'opacity': 1
		}, 500)
		this.isAnimating = true;
		setTimeout(() => {
            this.isAnimating = false;
		}, 500);
		this.step = 0;
	}
	
	moveLeft() {
		let self = this;
		let data;
		
        this.isAnimating = true;
        
		
		$('.collected:last').nextAll().each(function() {
			let index = $(this).index();
			let left = parseInt($(this).css('left'));
			let width = $(this).width();
			$(this).css('left', (left - (self.width + self.margin)) + 'px');
		});

		this.current.removeClass('current').addClass('collected');
		this.next.removeClass('next').addClass('current').next().addClass('next');

		data = $('.collected:last').data('constructor-item');
		

		$('.collected:last').css({
			// 'transform': `translate(${data.x}px, ${data.y}px)`,
			'margin-left': `${data.x}px`,
			'margin-top': `${data.y}px`,
			'z-index': data.zindex
		});

		if($('.collected').length == this.id.find('.constructor__item').length){
			$('.constructor-end').css({
				opacity: 1
			}, 300)
			setTimeout(function(){
				$(document).trigger('downTrue');
			})
		}
		

		this.current = this.next;
		this.next = this.next.next();
		
		setTimeout(() => {
			this.isAnimating = false;
		}, 500);

		
	}
	
	moveRight() {
		let self = this;
		
		$(document).trigger('downFalse');

		$('.constructor-end').css({
			opacity: 0
		}, 300)
		
		if($('.collected').length <= 1) {
			
			this.lstStep();
			
            setTimeout( function(){
                $(document).trigger('upTrue');
			})
			
			return false;
        }
		
		this.isAnimating = true;

		let $lastCollected = $('.collected:last');
		let width = $lastCollected.width();
		let index = $lastCollected.index();
		let left = parseInt($lastCollected.css('left'));

		$lastCollected.css({
			'left' : left + self.width + self.margin,
			// 'transform': 'translate(0px, 0px)',
			'margin-left': `0px`,
			'margin-top': `0px`,
			'z-index': '0'
		});


		$('.collected:last').nextAll().each(function() {
			let index = $(this).index();
			let left = parseInt($(this).css('left'));
			let width = $(this).width();
			$(this).css('left', (left + self.width + self.margin) + 'px');
		});

		this.current = $('.collected:last');
		this.next = $('.collected:last').next();

		$('.collected:last').removeClass('collected')
			.addClass('current')
			.next()
			.removeClass('current')
			.addClass('next')
			.next()
			.removeClass('next');
		
		$('.current').removeClass('next');
		
		
		
		setTimeout(() => {
			this.isAnimating = false;
		}, 500);
	}
	
	setCoords(width, margin) {
		for(let i = 0; i < this.itemsCount; i++) {
			let coord = 0;
			coord = (width + margin) * i;
			this.coords[i] = coord;
			$(this.elItem).eq(i).css('left', coord + 'px');
			
		}
	}
	
	events() {
		
		
		this.id.on('mousewheel', (event) => {
			let direction = this.getDirection(event);
			
			if(direction === true) {
				if(this.step === 0 && this.isAnimating === false) {
					this.lstStep();
				} else {
					if(this.isAnimating === false) {
						
						this.moveRight();
						
					} else {
						
						return false;
					}
				}
			} else {
				if(this.step === 0 && this.isAnimating === false) {

					this.firstStep();
					$(document).trigger('upFalse');

				} else {
					if(this.isAnimating === false) {

						this.moveLeft();
						
					} else {
						
						return false;
					}
				}
			}
		});
	}
}