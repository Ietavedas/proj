import Swiper from 'swiper';

export default class Stage_slider{
    constructor(){
        this.el = '.stage_list';

        this.init();
    }

    init(){
        const slider = new Swiper(this.el, {
            direction: 'vertical',
            spaceBetween: 30,
            mousewheel: {
				releaseOnEdges: true,
			},
			touchReleaseOnEdges: true,
            slidesPerView: 1,
            pagination: {
                el: '.stage_dots',
                clickable: true,
            },
        })
        
        this.counterAll();
        this.counterNow();

        const self = this;

        $('.stage_list').on('wheel', function(){
            let dots = $('.stage_dots').find('.swiper-pagination-bullet');
            let firstDot = dots[0];

            self.counterNow();
            
            if($(dots).first().hasClass('swiper-pagination-bullet-active')){
                setTimeout(function(){
                    $(document).trigger('upTrue');
                }, 500)
            }else{
                if(dots.last().hasClass('swiper-pagination-bullet-active')){
                    setTimeout(function(){
                        $(document).trigger('downTrue');
                    }, 500)
                }else{
                    $(document).trigger('downFalse');
                }
            }
            
        })

        $('.stage_dots').on('click', function(){
            let dots = $('.stage_dots').find('.swiper-pagination-bullet');
            let firstDot = dots[0];

            self.counterNow();
            
            if($(dots).first().hasClass('swiper-pagination-bullet-active')){
                setTimeout(function(){
                    $(document).trigger('upTrue');
                }, 500)
            }
            if(dots.last().hasClass('swiper-pagination-bullet-active')){
                setTimeout(function(){
                    $(document).trigger('downTrue');
                }, 500)
            }
        })
    }

    counterAll(){
        let countAll = $('.stage_dots').find('.swiper-pagination-bullet').length;
        if(countAll < 10){
            $('.stage_counter__all').html(`0${countAll}`)
        }else{
            $('.stage_counter__all').html(`${countAll}`)
        }
    }
    counterNow(){
        let countAll = $('.stage_dots').find('.swiper-pagination-bullet').length;
        let countNow = $('.stage_dots').find('.swiper-pagination-bullet-active').index() +1;

        if(countAll < 10){
            $('.stage_counter__now').html(`0${countNow}`);
        }else{
            $('.stage_counter__now').html(`${countNow}`);
        }
    }
}