import Swiper from 'swiper';

export default class Case_slider{
    constructor(){
        this.el = $('.card_list');

        this.init();
    }

    init(){
        // this.setWrapWidth(this.el)

        const slider = new Swiper(this.el, {
            // direction: 'vertical',
            mousewheel: {
				releaseOnEdges: true,
			},
			touchReleaseOnEdges: true,
            slidesPerView: 1,
            spaceBetween: 100,
            pagination: {
                el: '.card_dots',
                clickable: true,
            }
            // breakpoints: {
            //     1024: {
            //         spaceBetween: 0,
            //     }
            // }
        })

        slider.on('slideChange', function(){
            // let activeSlide = slider.activeIndex;
            // console.log(activeSlide)
            if(slider.isEnd){
                setTimeout(function(){
                    $(document).trigger('downTrue');
                }, 500)
            }
            if(!slider.isEnd){
                // setTimeout(function(){
                    
                // })
                $(document).trigger('downFalse');
            }
            if(slider.isBeginning){
                // console.log('isBeginning')
                
                setTimeout(function(){
                    $(document).trigger('upTrue');
                }, 500)
            }
            if(!slider.isBeginning){
                // console.log('isBeginning')
                
                $(document).trigger('upFalse');
            }
            console.log(slider.activeIndex)
        })

        // setTimeout(function(){
            
        // })
    }

    setWrapWidth(el){
        let child = el.find('.card');
        let childWidth = child.outerWidth();
        let margin = 100 * child.length;
        
        // el.css({
        //     // 'position': 'absolute',
        //     'width': `${childWidth * child.length + 1000}`
        // })
        // console.log(childWidth)
    }
}