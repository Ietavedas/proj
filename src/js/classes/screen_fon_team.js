export default class Screen_fon_team{
    constructor(){
        this.block = '.js-team';
        this.isAnimating = false;

        this.bg;

        this.init()
    }
    
    init(){ 
               
        $(this.block).on('mouseenter', (event) => {

            clearTimeout(this.bg);

            let count = this.count(event);
            this.render(count)
        });

        $(this.block).on('mouseleave', (event) => {
            $('.team__fon').removeClass('team__fon--amimate')
        });

        $(this.block).on('click', function(){
            
            $(this).addClass('team--active').siblings().removeClass('team--active');

            $('.overflow').css({
                'display': 'block'
            })
            $('.overflow').animate({
                'opacity': 1
            }, 500)

            $('.left_popup').animate({
                'right': 0
            }, 500)

            let title = $(this).data('title');
            let text = $(this).data('text');

            $('.left_popup').find('.h2').html(title);
            $('.left_popup').find('.left_popup__text').html(text)

        })
        
        $('.overflow').on('click', function(){
            $('.js-team').removeClass('team--active')
            setTimeout(function(){
                $('.overflow').css({
                    'display': 'none'
                })
            }, 500)
            $(this).animate({
                'opacity': 0
            }, 500)

            $('.left_popup').animate({
                'right': '-100%'
            }, 500)

            $('.left_popup').find('.h2').html('');
            $('.left_popup').find('.left_popup__text').html('')
        })
    }

    count({currentTarget}){
        const selector = $(currentTarget);

        return selector.data('fon');
    }

    render(count){
        $('.team__fon').append(`<div class="team__fon--amimate" style="background: url(img/5bg-fon-${count}.jpg) center center no-repeat" ></div>`)
        
        let x = $('.team__fon').find('div');
        
        this.bg = setTimeout( () => {

            $('.team__fon').css({
                background: `url(img/5bg-fon-${count}.jpg) center center no-repeat`
            })

        }, 900 )

        setTimeout( () => {
            x.remove();
        }, 10000 )
    }
}