import 'slick-carousel';
// import Tab_carousel from './tab_carousel';

export default class Tabs {
    constructor(){
        this.el = '.stage__tab';

        this.init(this.el);
    }

    init(el){
        $(el).on('click', function() {
            // console.log($(el));
            $(this).addClass('stage__tab--active').siblings().removeClass('stage__tab--active');

            let ind = $(this).index();
            
            $(this)
                .parents('.stage')
                .find('.stage__section')
                .eq(ind)
                .addClass('stage__section--active')
                .siblings()
                .removeClass('stage__section--active');
            
            
            if($(this).parents('.stage').find('.stage__section').eq(ind).find('.goods_slider').length > 0){
                $(this).parents('.stage').find('.stage__section').eq(ind).find('.goods_slider').slick({
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    dots: true,
                    appendDots: $(this).parents('.stage').find('.stage__section').eq(ind).find('.dots_wrap_slick'),
                    dotsClass: 'dots',
                    prevArrow: '<div class="arrPrev"><div class="arrPrev__iside"></div></div>',
                    nextArrow: '<div class="arrNext"><div class="arrNext__iside"></div></div>'
                });
            }
        })
    }
}