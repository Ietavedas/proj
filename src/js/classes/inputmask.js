import Inputmask from "inputmask";

export default class Mask extends Inputmask{
    constructor(){
        super()
        this.el = $('.js-mask');

        this.init();
    }

    init(){
        Inputmask("+7 ( 9 9 9 ) 9 9 9 - 9 9 - 9 9").mask(this.el);
    }
}