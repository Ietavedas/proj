export default class Logo_slider{
    constructor(){
        this.el = $('.logo_slider');
        this.dotsContainer = $('.logo_slider__dots');
        this.params = {
            desctop: {
                width: 1280,
                count: 12
            },
            mobile: {
                width: 1024,
                count: 6
            }
        }

        this.isAnimating = [];

        this.init();
        this.reinit();
    }

    reinit(){
        $(window).on('resize', () => {
            // let windowWidth = $(window).outerWidth();
            this.init();
        });
    }

    init(){
        let windowWidth = $(window).outerWidth();
        let devise;   
        
        

        if(windowWidth >= this.params.desctop.width){
            devise = this.params.desctop;
            
        }

        if(windowWidth < this.params.mobile.width){
            devise = this.params.mobile;
            
        }
        
        this.addClass(devise);
        let dot = this.calculateDots(devise);

        this.removeDots();

        if(dot > 1){
            for(let i = 1; i <= dot; i++){
                // this.el.insertAdjacentHTML('beforeend', `<div class="logo_slider__dots">${this.renderDots()}</div>`)
                // setTimeout( () => {
                    
                // }, 3000)
                this.dotsContainer[0].insertAdjacentHTML('beforeend', this.renderDots(i))
            }
            
        }

        this.dotsContainer.children().on('click', (event) => {
            // console.log($(event.target).index())
            
            this.removeClass();
            
            this.changeClasses(devise, event)
            
            $(event.target).addClass('logo_slider__dot--active').siblings().removeClass('logo_slider__dot--active');
        })
    }

    addClass(devise){
        let timer = 300;
        // this.isAnimating = true;
        // this.el.children().clearQueue()
        this.el.children().each( (index, element) =>{

            let ind = index + 1;

            if(ind <= devise.count){
                
                $(element).addClass('logo_slider__slide--animate')
                this.isAnimating.push(window.setTimeout( function(){
                    // if(this.isAnimating == true){
                        
                    // }
                    
                }, index * timer  ))

            }
        })
    }

    changeClasses(devise, event){
        let timer = 300;
        // this.isAnimating = true;

        let prevFactor = $(event.target).index();
        let factor = prevFactor + 1;

        let prevSumm = devise.count * prevFactor;
        let summ = devise.count * factor;

        console.log(prevFactor)

        this.isAnimating = [];

        this.el.children().each( (index, element) =>{

            let ind = index + 1;

            if(ind > prevSumm && ind <= summ){

                $(element).addClass('logo_slider__slide--animate')

                this.isAnimating.push(window.setTimeout( function(){
                    
                        
                        
                        
                    }, index * timer  )
                )

                
                
            }
        })
        
    }

    removeClass(){
        // let timer = 300;
        // this.isAnimating = false;
        console.log(this.isAnimating)
        this.isAnimating.map( (item) =>{
            window.clearTimeout(item)
        } )
        

        this.el.children().each( (index, element) => {
            // console.log(element)
            // setTimeout( () => {
                
            // }, index * timer )
            
            $(element).removeClass('logo_slider__slide--animate');
        })
    }

    calculateDots(devise){
        let count = this.el.children().length / devise.count;
        return Math.floor(count) + 1;        
    }

    renderDots(i){
        if(i == 1){
            return(`
                <div class="logo_slider__dot logo_slider__dot--active"></div>
            `)
        }else{
            return(`
                <div class="logo_slider__dot"></div>
            `)
        }
        
    }

    removeDots(){
        if( this.dotsContainer.children().length > 0){
            $('.logo_slider__dots').children().remove();
        }
    }


    
}