// Load plugins
import 'jquery';

import '@fancyapps/fancybox';


import svg4everybody from 'svg4everybody'
window.svg4everybody = svg4everybody;

//Load modules
import SvgUse from './classes/svgUse';
import './classes/polyfills';
import Menu from './classes/menu';
// import Fullpage from './classes/fullpage';
import Fp from './classes/fullpage.jqm';
import SliderConstructor from './classes/slider_constructor';
import DC from './classes/GLOBAL_CONTROLLER';
import Screen_fon from './classes/screen_fon';
import Screen_fon_team from './classes/screen_fon_team';
import customScroll from './classes/scrollpane';
import Case_slider from './classes/case_slider';
import Inputmask from './classes/inputmask';

import Logo_slider from './classes/logo_slider';
import Tabs from './classes/stage_tab';
// import Tab_carousel from './classes/tab_carousel';
import Stage_slider from './classes/stage_slider';

import Sliders from './classes/slider';
import Popup from './classes/popup';
// import Class from './classes/file';

// Run components

window.App = {
    debug: false,
    // debug: true,
    lang: 'ru'
};

// debug detect

if (window.location.href.indexOf('.ru') !== -1 || window.location.href.indexOf('/en') !== -1) {
    App.debug = false;
}

if (window.SITE_LANG) {
    App.lang = window.SITE_LANG;
}

if (App.debug) {
    console.log('Debug: ' + App.debug);
}

// $(window).on('load', function () {
//     App.Preloader = new Preloader();
// });

document.addEventListener('DOMContentLoaded', () => {
    
    App.SvgUse = new SvgUse();
    
    App.Fp = new Fp();
    // App.Fullpage = new Fullpage();
    App.Sliders = new Sliders();
    App.SliderConstructor = new SliderConstructor();
    App.Screen_fon = new Screen_fon();
    App.customScroll = new customScroll();
    App.Case_slider = new Case_slider();
    App.Inputmask = new Inputmask();
    App.Logo_slider = new Logo_slider();
    App.Tabs = new Tabs();
    App.Stage_slider = new Stage_slider();
    App.Screen_fon_team = new Screen_fon_team();
    // App.Tab_carousel = new Tab_carousel();
    App.Menu = new Menu();
    
    App.Popup = new Popup();
});

$('.embed-container').find('iframe').find('.play').css('display', 'none');